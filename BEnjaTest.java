package inf234;

import org.junit.Test;

import static org.junit.Assert.*;

public class MiseEnJambeTest {

    @Test
    public void testContient1() throws Exception {
        MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
        assertTrue(m1.contient("uvw</"));
    }
    
    public void testContient2() throws Exception {
        MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
        assertTrue(m1.contient("o"));
    }


    public void testContient3() throws Exception {
        MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
        assertTrue(m1.contient("<a>xyz<b>uvw</b>toto</a>"));
    }

    public void testContient4() throws Exception {
        MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
        assertTrue(m1.contient(""));
    }

    public void testContientBalise1() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
            assertTrue(m1.contientBalise("b"));
    }
    public void testContientBalise2() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a attr="5">xyz<b>uvw</b>toto</a>");
            assertTrue(m1.contientBalise("a"));
    }

    public void testContientBalise3() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a attr="5">xyz<b>uvw</b>toto</a>");
            !(assertTrue(m1.contientBalise("c")));
    }





    public void testContenu1() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<b>uvw</b>");
            assertTrue(m1.contientContenu("uvw"));
    }

    public void testContenu2() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
            assertTrue(m1.contientContenu("xyzuvwtoto"));
    }

    public void testSansCommentaire1() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
            assertTrue(m1.sansCommentaire()==("<a>xyz<b>uvw</b>toto</a>"));
    }

    public void testSansCommentaire2() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b><!-- blabla -->uvw</b>toto</a>");
            assertTrue(m1.sansCommentaire()==("<a>xyz<b>uvw</b>toto</a>"));
    }
    
    public void testSansCommentaire3() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a><!-- blabla -->xyz<b>uvw</b>toto<!-- blabla --></a>");
            assertTrue(m1.sansCommentaire()==("<a>xyz<b>uvw</b>toto</a>"));
    }


    public void filtrer1() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
            assertTrue(m1.filtrer("b")==("<a>xyztoto</a>"));
    }

    public void filtrer2() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a>");
            assertTrue(m1.filtrer("a")==(""));
    }

    public void filtrer2() throws Exception {
            MiseEnJambe m1 = new MiseEnJambe("<a>xyz<b>uvw</b>toto</a><c>tata</c>");
            assertTrue(m1.filtrer("a")==("<c>tata</c>");
    }


}
